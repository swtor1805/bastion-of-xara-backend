import { Injectable } from '@nestjs/common';
import { SavegameDto } from './savegame.dto';
import { SaveGame } from './savegame.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from 'src/user/user.entity';

@Injectable()
export class SavegameService {
    constructor(
        @InjectRepository(SaveGame)
        private savegameRepository: Repository<SaveGame>
    ) {}

    loadgame(data: SavegameDto) {
        return this.savegameRepository.createQueryBuilder('savegame')
            .innerJoin('savegame.user', 'user')
            .where("user.id = :id", {id: data.userId})
            .where("savegame.saveNumber = :number", {number: data.SaveNumber})
            .getOne();
    }
    
    savegame(data: SavegameDto) {
        console.log(data);
        return this.savegameRepository.createQueryBuilder('savegame')
            .innerJoin('savegame.user', 'user')
            .where("user.id = :id", {id: data.userId})
            .where("savegame.saveNumber = :number", {number: data.SaveNumber})
            .getOne().then(save => {
                if (!save) {
                    console.log(`Create new Savegame at file ${data.SaveNumber}`)
                    save = new SaveGame();
                    const user = new User();
                    user.id = data.userId;
                    save.user = user;
                }
                save.saveNumber = data.SaveNumber;
                save.currentLv = data.PlayerLvl;
                save.totalKills = data.Kills;
                save.totalDeaths = 0;
                console.log("Game saved?")
                return this.savegameRepository.save(save);
            });
    }
}
