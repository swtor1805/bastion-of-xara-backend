import {Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, OneToOne, JoinColumn, ManyToOne} from "typeorm";
import { User } from "src/user/user.entity";

@Entity()
export class SaveGame {

    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => User, user => user.savegames)
    user: User;

    @Column()
    currentLv: number;

    @Column()
    totalKills: number;

    @Column()
    totalDeaths: number;

    @Column()
    saveNumber: number;

    @CreateDateColumn()
    creationDate: Date;
}