import { Test, TestingModule } from '@nestjs/testing';
import { SavegameController } from './savegame.controller';

describe('Savegame Controller', () => {
  let controller: SavegameController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SavegameController],
    }).compile();

    controller = module.get<SavegameController>(SavegameController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
