import { Levels } from "src/enums/Levels.enum";

export class SavegameDto {
    userId: number;
    PlayerLvl: number;
    Kills: number;
    SaveNumber: number;
}