import { Module } from '@nestjs/common';
import { SavegameService } from './savegame.service';
import { SavegameController } from './savegame.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SaveGame } from './savegame.entity';
import { UserModule } from 'src/user/user.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([SaveGame])
  ],
  providers: [SavegameService],
  controllers: [SavegameController],
  exports: [
    TypeOrmModule.forFeature([SaveGame]),
    SavegameService
  ]
})
export class SavegameModule {}
