import { Test, TestingModule } from '@nestjs/testing';
import { SavegameService } from './savegame.service';

describe('SavegameService', () => {
  let service: SavegameService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SavegameService],
    }).compile();

    service = module.get<SavegameService>(SavegameService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
