import { Controller, Post, Body, Get, Param } from '@nestjs/common';
import { SavegameService } from './savegame.service';
import { SavegameDto } from './savegame.dto';

@Controller('savegame')
export class SavegameController {

    constructor(private service: SavegameService) {
    }

    @Post()
    savegame(@Body() data: SavegameDto) {
        return this.service.savegame(data);
    }

    @Post('load')
    async loadGame(@Body() data: SavegameDto) {
        const savegame = await this.service.loadgame(data);
        const dto = new SavegameDto();
        dto.Kills = savegame.totalKills;
        dto.PlayerLvl = savegame.currentLv;
        return dto;
    }
}
