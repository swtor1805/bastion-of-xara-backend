import { Controller, Get, Res } from '@nestjs/common';
import { AppService } from './app.service';
import { Response } from 'express';
import * as fs from 'fs';
import { Readable } from 'typeorm/platform/PlatformTools';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Get('download')
  loadgamefiles(@Res() res: Response) {
    const buffer = fs.readFileSync('./gamefiles/Bastion_of_Xara_v0.5.2.zip');
    const stream = new Readable();
    stream.push(buffer);
    stream.push(null);
    res.set({
      'Content-Type': 'application/zip',
      'Content-Length': buffer.length,
      'Content-Disposition': 'attachement; filename=Bastion_of_Xara_v0.5.2.zip'
    });
    stream.pipe(res);
  }
}
