import {Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, OneToOne, OneToMany} from "typeorm";
import { SaveGame } from "src/savegame/savegame.entity";

@Entity()
export class User {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    username: string;

    @Column()
    password: string;

    @Column()
    ownsGame: boolean;

    @Column()
    email: string;

    @OneToMany( type =>SaveGame, savegame => savegame.user )
    savegames: SaveGame[];

    @CreateDateColumn()
    creationDate: Date;
}