import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './user.entity';
import { JwtModule } from '@nestjs/jwt'
import { PassportModule } from '@nestjs/passport';

@Module({
  imports: [
    TypeOrmModule.forFeature([User]),
    PassportModule.register({
      defaultStrategy: 'jwt',
      property: 'user',
      session: false
    }),
    JwtModule.register({
      secret: 'd&sd98fd&$$$',
      signOptions: {expiresIn: '8h'}
    })
  ],
  providers: [UserService],
  controllers: [UserController],
  exports: [
    TypeOrmModule.forFeature([User]),
    UserService
  ]
})
export class UserModule {}
