import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './user.entity';
import { Repository } from 'typeorm';
import { loginUser } from './objects/loginUser.class';
import { registerUser } from './objects/registerUser.class';
import { userResponse } from './objects/userRepsonse.class';
import { JwtService } from '@nestjs/jwt'
import * as bcrypt from 'bcrypt'; 

@Injectable()
export class UserService {

    constructor(
        @InjectRepository(User)
        private userRepository: Repository<User>,
        private jwtService: JwtService
    ) {}

    findUserByID(id) {
        return this.userRepository.findOne(id);
    }

    buyGame(data: any) {
        return this.userRepository.findOne(data.userId).then((user: User) => {
            user.ownsGame = true;
            return this.userRepository.save(user).then(() => {
                return true;
            })
        })
    }

    login(user: loginUser) : Promise<any> {
        console.log(user);
        return this.userRepository.findOne({username: user.username}).then(userExists => {
            if (userExists) {
                if (bcrypt.compareSync(user.password, userExists.password)) {
                    let newUser = new loginUser();
                    newUser.userId = userExists.id;
                    newUser.username = userExists.username;
                    newUser.ownsGame = userExists.ownsGame;
                    const payload = {username: userExists.username, userId: userExists.id, ownsGame: userExists.ownsGame};
                    newUser.access_token = this.jwtService.sign(payload);
                    console.log(newUser);
                    return newUser;
                }
            }
        });
    }

    register(user: registerUser) : Promise<userResponse> {
        let response: userResponse = new userResponse();
        console.log(user);

        if(user.password !== user.passwordTwo) {
            response.isValid = false;
            response.info = "Passwoerter muessen uebereinstimmen";

            return new Promise((res, rej) => { res(response)});
        }

        return this.userRepository.findOne({username: user.username}).then( userExists => {

            if(userExists) {
                response.isValid = false;
                response.info = "Benutezrname ist bereits vorhanden";

                return new Promise((res, rej) => { res(response)});
            }

            
            
            const salt = bcrypt.genSaltSync(10);
            const hash = bcrypt.hashSync(user.password, salt);

            let newUser: User = new User();

            newUser.username = user.username;
            newUser.password = hash;
            newUser.ownsGame = false;
            newUser.email = user.email;

            return this.userRepository.save(newUser).then(() => {
                response.isValid = true;
                response.info = '';

                return response
            })
        });
    }
}
