import { Controller, Post, Body } from '@nestjs/common';
import { UserService } from './user.service';
import { loginUser } from './objects/loginUser.class';
import { registerUser } from './objects/registerUser.class';
import { userResponse } from './objects/userRepsonse.class';

@Controller('user')
export class UserController {

    constructor(private userService: UserService) {}

    @Post('login')
    login(@Body() user: loginUser) : Promise<any> {
        return this.userService.login(user);
    }

    @Post('register')
    register(@Body() user: registerUser) : Promise<userResponse> {
        return this.userService.register(user);
    }

    @Post('buygame')
    buyGame(@Body() data: any) : Promise<boolean> {
        return this.userService.buyGame(data);
    }
}
