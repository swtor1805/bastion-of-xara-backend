export class loginUser {
    userId: number;
    username: string;
    password: string;
    ownsGame: boolean;
    access_token: string;
}